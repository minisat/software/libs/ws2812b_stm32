#include "sg_ws2812b.h"

inline void write1(){
	SG_WS2812_GPIOx->BSRR = SG_WS2812_GPIO_PINx;
	asm("nop");
	asm("nop");
	asm("nop");
	asm("nop");
	SG_WS2812_GPIOx->BRR = SG_WS2812_GPIO_PINx;
	asm("nop");
}
inline void write0(){
	SG_WS2812_GPIOx->BSRR = SG_WS2812_GPIO_PINx;
	SG_WS2812_GPIOx->BRR = SG_WS2812_GPIO_PINx;
	asm("nop");
}

void sg_ws2812b_write(uint32_t data){
	for (uint32_t i=0; i<24; i++){
		if (data & 0x800000)
			write1();
		else
			write0();
		data<<=1;
	}
}
