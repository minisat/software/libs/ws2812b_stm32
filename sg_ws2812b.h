#ifndef SG_WS2812B_H
#define SG_WS2812B_H

#include "stm32f1xx_hal.h"
#ifdef SG_CONFIG
#include "sg_config.h"
#endif

#ifndef 
#define SG_WS2812_GPIOx			GPIOA
#warning SG_WS2812_GPIOx defined by default
#endif
#ifndef 
#define SG_WS2812_GPIO_PINx		GPIO_PIN_8
#warning SG_WS2812_GPIO_PINx defined by default
#endif

void sg_ws2812b_write(uint32_t data);


#endif